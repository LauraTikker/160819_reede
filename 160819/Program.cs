﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _160819
{
    class Program
    {
        static void Main(string[] args)
        {
            //FAILID:

            string failinimi = @"..\..\hommikunetext.txt"; // ka saab kui \ on topet ..=> kaust ülespoole (projekti kaustas)
            //string nimi = Console.ReadLine();
            //string muster = $@"c:\henn\{nimi}.txt";

            var loetud = File.ReadAllText(failinimi); // avab, loeb, suleb
            Console.WriteLine(loetud);

            var loetudRead = File.ReadAllLines(failinimi); // annab stringimassiivi igast reast, reavahetuse kohas
            for (int i = 0; i < loetudRead.Length; i++)
            {
                Console.WriteLine($"Rida {i} -- {loetudRead[i]}");
            }
            string uusfailinimi = @"..\..\uusnimekiri.txt";
            File.WriteAllLines(uusfailinimi, loetudRead);
            string teinefail = @"..\..\teinenimekiri.txt";

            string[] loetelu = new string[] { "koos,", "kutsa", "lõvi", "keemia" }; //loob uue stringi massiivi
            File.WriteAllLines(teinefail, loetelu); // kirjutab kõik erinevale reale

            // KOLMAS FAIL, MILLEST VÕETAKSE ANDMED NIMI JA VANUS:

            string loomadeNimekiri = @"..\..\loomadenimekiri.txt"; // siis ta teab texti faili
            var loomaLoetud = File.ReadAllLines(loomadeNimekiri); // loeb faili ja teeb Stringi Array, nimi ja vanus on ühe stringina: Merisiga 1

            Dictionary<string, int> nimekiri = new Dictionary<string, int>();
            foreach (var x in loomaLoetud)
            {
                var jupid = x.Split(' ');
                string nimi = jupid[0];
                int vanus = int.Parse(jupid[1]);
                nimekiri.Add(nimi, vanus);
            }

            foreach (var x in nimekiri) Console.WriteLine(x);
            Console.WriteLine($"keskmine vanus on {nimekiri.Values.Average()}");

            //RANDOM:

            Random r = new Random();
            r.Next(100); // annab juhusliku täisarvu 0 kuni 100 (piir)
            r.NextDouble(); // annab juhusliku double-arvu 0<...<1

            // segada 52 arvu:

            List<int> arvud = new List<int>();

            Random r = new Random();
            int uus = r.Next(52);
            while (arvud.Count < 52)
            {
                int number = r.Next(52);
                if (!arvud.Contains(number))
                {
                    arvud.Add(number);
                }
            }
            foreach (var x in arvud) Console.WriteLine(x);
            Console.WriteLine(arvud.Count());

            // Stringide muutmine

            string nimi = "";
            Console.WriteLine("Sisesta nimi: ");
            nimi = Console.ReadLine();
            var täisnimi = nimi.Split(' ');
            string eesnimi = täisnimi[0].ToLower();
            string taganimi = täisnimi[1].ToLower();
            char esimene = eesnimi.First();
            char teine = taganimi.First();
            string Uesimene = esimene.ToString().ToUpper();
            string Uteine = teine.ToString().ToUpper();
            eesnimi = eesnimi.Replace((eesnimi.First()), Uesimene.First());
            taganimi = taganimi.Replace((taganimi.First()), Uteine.First());
            Console.WriteLine(eesnimi + " " + taganimi);

            // SAAB KA (õpetajalt) poolik
            :

            string nimi = "";
            do
            {
                Console.WriteLine("Sisesta nimi: ");
                nimi = Console.ReadLine();
                if (nimi != "")
                {
                    var nimed = nimi.Split(' ');

                    for (int i = 0; i < nimed.Length; i++)
                        if(nimed[i] != "")
                    {

                    }
                }
            }


        }
    }
}
